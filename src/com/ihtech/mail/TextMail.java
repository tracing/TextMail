package com.ihtech.mail;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class TextMail {

	private String host;
	private String prtl;
	private String username;
	private String password;

	public TextMail() {
		this.host = "smtp.163.com";
		this.prtl = "smtp";
		this.username = null;
		this.password = null;
	}

	public TextMail(String host, String prtl, String username, String password) {
		this.host = host;
		this.prtl = prtl;
		this.username = username;
		this.password = password;
	}

	/**
	 * @param from
	 *            发件人
	 * @param to
	 *            收件人
	 * @param subject
	 *            主题
	 * @param content
	 *            邮件内容
	 */
	public void sendTextMail(String from, String[] to, String subject,
			String content) throws Exception {
		Properties prop = new Properties();
		prop.setProperty("mail.host", host);
		prop.setProperty("mail.transport.protocol", prtl);
		prop.setProperty("mail.smtp.auth", "true");
		// 使用JavaMail发送邮件的5个步骤
		// 1、创建session
		Session session = Session.getInstance(prop);
		// 开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
		session.setDebug(true);
		// 2、通过session得到transport对象
		Transport ts = session.getTransport();
		// 3、使用邮箱的用户名和密码连上邮件服务器，发送邮件时，发件人需要提交邮箱的用户名和密码给smtp服务器，
		//用户名和密码都通过验证之后才能够正常发送邮件给收件人。
		ts.connect(host, username, password);
		// 4、创建邮件
		Message message = createTextMail(session, from, to, subject, content);
		// 5、发送邮件
		ts.sendMessage(message, message.getAllRecipients());
		ts.close();
	}

	public static MimeMessage createTextMail(Session session, String from,
			String[] to, String subject, String content) throws Exception {
		Address[] addr = new Address[to.length];
		for (int i = 0; i < to.length; i++) {
			addr[i] = new InternetAddress(to[i]);
		}
		// 创建邮件对象
		MimeMessage message = new MimeMessage(session);
		// 指明邮件的发件人
		message.setFrom(new InternetAddress(from));
		// 指明邮件的收件人
		message.setRecipients(Message.RecipientType.TO, addr);
		// 邮件的标题
		message.setSubject(subject);
		// 邮件的文本内容
		message.setContent(content, "text/html;charset=UTF-8");
		// 返回创建好的邮件对象
		return message;
	}
}
